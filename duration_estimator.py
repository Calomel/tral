import sklearn.ensemble
import sklearn.svm
import sklearn.naive_bayes
import sklearn.model_selection
import sklearn.metrics
import sklearn.utils
import scipy.io
import seaborn
import numpy
import importlib
import pickle
import random
import matplotlib.pyplot as plt

import tral.const
import tral.metrics

mat = scipy.io.loadmat("mat/cache/duration_estimator.samples.mat")
x_train, y_train, w_train = mat["x_train"], mat["y_train"][0], mat["w_train"][0]
x_val, y_val = mat["x_val"], mat["y_val"][0]
class_weight_train = dict([(int(x), y) for x,y in mat["class_weight_train"]])


print("Train: {}, {}".format(x_train.shape, y_train.shape))
print("Val: {}, {}".format(x_val.shape, y_val.shape))
print("Weights: {}".format(class_weight_train))

names = [str(x) for x in tral.const.DURATION_LIST]


### Execute the model

model_random_forest =  sklearn.ensemble.RandomForestClassifier(n_estimators=1024, n_jobs=16, max_depth=7, random_state=0,
                                                 ccp_alpha=0, criterion='entropy')
model_random_forest.fit(x_train, y_train, w_train)


y_pred = model_random_forest.predict(x_val)
_,_ = tral.metrics.confusion_matrix(y_val, y_pred, names, class_weight=class_weight_train, annot=True, figsize=(14,10))