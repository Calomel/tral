import keras.losses as KF
import keras.backend as KB
import keras.backend.tensorflow_backend as KBT
import keras.metrics as KM
import keras.callbacks as KC
import tensorflow
import collections
import matplotlib.pyplot as pyplot
import pathlib
import sklearn.metrics as SM
import sklearn.utils as SU
import numpy as N
import seaborn

import pandas

def plot_history(h, key):
    pyplot.plot(h[key])
    pyplot.plot(h['val_' + key])
    pyplot.title(key)
    pyplot.xlabel('Epoch')
    pyplot.legend(['Train', 'Validation'], loc='upper left')
    pyplot.show()

def weighted_binary_crossentropy(target, output):
    """
    Weighted binary crossentropy between an output tensor 
    and a target tensor. POS_WEIGHT is used as a multiplier 
    for the positive targets.

    Combination of the following functions:
    * keras.losses.binary_crossentropy
    * keras.backend.tensorflow_backend.binary_crossentropy
    * tf.nn.weighted_cross_entropy_with_logits
    """
    POS_WEIGHT = 128/3
    # transform back to logits
    _epsilon = KBT._to_tensor(KBT.epsilon(), output.dtype.base_dtype)
    output = tensorflow.clip_by_value(output, _epsilon, 1 - _epsilon)
    output = KB.log(output / (1 - output))
    # compute weighted loss
    loss = tensorflow.nn.weighted_cross_entropy_with_logits(target, output, pos_weight=POS_WEIGHT)
    return tensorflow.reduce_mean(loss, axis=-1)



def note_loss(y_true, y_pred):
    return KF.sparse_categorical_crossentropy(y_true, y_pred, axis=-1)

def note_accuracy(y_true, y_pred):
    return KM.sparse_categorical_accuracy(y_true, y_pred, axis=-1)

    y_true = KB.cast(y_true, KB.floatx())
    y_pred = KB.argmax(y_pred, axis=-1)
    y_pred = KB.cast(y_pred, KB.floatx())
    
    result = KB.equal(y_true, y_pred)
    result = KB.cast(result, KB.floatx())
    result = KB.mean(result)
    return result


def callback_modelsaver(name):
    fpath = "models/" + name + "/model.{epoch:02d}.hdf5"
    return KC.callbacks.ModelCheckpoint(fpath, verbose=0, save_best_only=False, save_weights_only=False, mode='auto', period=1)

def callback_modelhist(name):
    fpath = "models/" + name + "/history.csv"
    return KC.callbacks.CSVLogger(fpath, separator=',', append=True)

def callbacks(name):
    pathlib.Path("models/" + name).mkdir(parents=True, exist_ok=True)
    return [callback_modelsaver(name), callback_modelhist(name)]

def generate_class_weights(y):
    x = dict(collections.Counter(y))
    max_val = max(x.values())
    for k in x.keys():
        x[k] = max_val / x[k]
    return x
        
    
def confusion_matrix(y_true, y_pred, names, normalize=True, class_weight=None, \
                     silent=False, annot=False, figsize=(10,7), output_dict=False, cmap='Blues'):
    # Generate sample weights
    if class_weight:
        sample_weight = SU.class_weight.compute_sample_weight(class_weight, y_true)
        class_report = SM.classification_report(y_true, y_pred, sample_weight=sample_weight, target_names=names,\
                                                output_dict=output_dict)
    else:
        class_report = SM.classification_report(y_true, y_pred, target_names=names,\
                                                output_dict=output_dict)
    
    confm = SM.confusion_matrix(y_true, y_pred)
    if normalize:
        row_sums = confm.sum(axis=-1)
        confm = confm / row_sums[:, N.newaxis]
        
    df_cm = pandas.DataFrame(confm, index=names, columns=names)
    
    if not silent:
        if not output_dict:
            print(class_report)
        fig,ax = pyplot.subplots(figsize=figsize)
        seaborn.heatmap(df_cm, annot=annot, ax=ax, cmap=cmap, square=True)
        ax.set_xlabel("Predicted")
        ax.set_ylabel("Actual")
        fig.show()
    
    return class_report, confm
        
       