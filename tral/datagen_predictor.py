import numpy as N
import tensorflow.keras.utils as KU
import pickle
import scipy.io
from .const import *

class DataGenPredictor(KU.Sequence):
    
    def __init__(self, files, \
                 batchsize = 16, \
                 permissible_durations=DURATION_LIST, \
                 mask=-1, \
                 binary=True):
        """
        Binary: If true, all note durations are erased
        """
        self.files = files
        self.batchsize = batchsize
        self.permissible_durations = [0] + DURATION_LIST
        self.activate_convert_function = N.vectorize(self.permissible_durations.index)
        self.mask = mask
        self.binary = binary
    
    def int_ceildiv(self, a: int, b: int):
        return -((-a) // b)
    
    def __len__(self):
        return self.int_ceildiv(len(self.files), self.batchsize)
    
    def __getitem__(self, idx):
        start_idx = idx * self.batchsize
        mats = [self.readone(x) for x in self.files[start_idx:start_idx+self.batchsize]]
        
        assert len(mats) <= self.batchsize
        
        out_x = self.justified_merge([x for x,y in mats])
        out_y = self.justified_merge([y for x,y in mats])

        return out_x, out_y
    
    def justified_merge(self, li, truncate=None):
        if not truncate:
            truncate = max([x[0].shape[0] for x in li])
        
        li = [N.expand_dims(x,axis=0) for x in li]
        def padone(x):
            if truncate > x.shape[1]:
                pad = truncate - x.shape[1]
                return N.pad(x, ((0,0),(0,pad),(0,0)), 'constant', constant_values=self.mask)
            elif truncate < x.shape[1]:
                return x[:,:truncate,:]
            else:
                return x
        
        result = N.concatenate([padone(x) for x in li])
        assert result.shape[1] == truncate
        return result
    
    def readone(self, f):
        mat = scipy.io.loadmat(f)
        m_status = mat["status"]
        m_activate = mat["activate"]
        if self.binary:
            m_activate = N.clip(m_activate, 0, 1)
        
        m_activate = self.activate_convert_function(m_activate)
        
        
        zeros = N.zeros((1,PITCH_MAX),dtype='int8')
        m_status = N.concatenate([N.zeros((1,PITCH_MAX),dtype='int8'), m_status], axis=0)
        m_activate = N.concatenate([m_activate, N.zeros((1,PITCH_MAX),dtype='int16')], axis=0)
        
        # Normalise activator
        
        return m_status, m_activate
        