import numpy as N
import numpy.random as NR
import librosa as R
import tensorflow.keras.utils as KU
import scipy.io
import tral.instruments
import random
from .const import *

def load_instrument_spectra(voice, duration, fpath="mat/instruments/", truncate=128):
    mat = scipy.io.loadmat(fpath + "v{}d{}.mat".format(voice, duration))
    mat = mat["spectra"]
    mat = N.abs(mat)
    #mat = R.amplitude_to_db(mat, ref=N.max)
    
    if truncate is not None:
        if truncate > mat.shape[1]:
            pad = truncate - mat.shape[1]
            return N.pad(mat, ((0,0),(0,pad),(0,0)), 'constant', constant_values=0)
        elif truncate < mat.shape[1]:
            return mat[:,:truncate,:]
        else:
            return mat
    else:
        return mat
    
def spectrum_regularise(spec):
    return  - (spec / 80)
def spectrum_unregularise(spec):
    return ( -spec) * 80

class DataGenAutoencoder(KU.Sequence):
    
    def __init__(self, fpath="mat/instruments/", \
                 instrument_range=tral.instruments.LIST, \
                 pitch_range=range(128), \
                 pitch_to_categorical = False, \
                 fix_input_pitch=False, \
                 batchsize = 16, \
                 truncate = 128, \
                 spectrum_db = True, \
                 duration = 240, \
                 seed = 0):
        
        self.instrument_range = instrument_range
        self.pitch_range = pitch_range
        self.pitch_to_categorical = pitch_to_categorical
        self.fix_input_pitch=fix_input_pitch
        self.truncate = truncate
        self.duration = duration
        self.batchsize = batchsize
        self.spectrum_db = spectrum_db
        
        # Load all instruments
        self.data = [(v,load_instrument_spectra(v, self.duration, fpath=fpath)) for v in self.instrument_range]
        self.data = dict(self.data)
        
        random.seed(seed)
        self.index_list = list(range(self.len_samples))
        random.shuffle(self.index_list)
    
    @property
    def len_samples(self):
        if self.fix_input_pitch:
            return len(self.instrument_range) * len(self.pitch_range)
        else:
            return len(self.instrument_range) * len(self.pitch_range) * len(self.pitch_range)
        
    def __len__(self):
        def int_ceildiv(a: int, b: int):
            return -((-a) // b)
        return int_ceildiv(self.len_samples, self.batchsize)
    
    def __getitem__(self, idx):
        samples = [self.index_list[idx * self.batchsize + i] \
                   for i in range(self.batchsize) \
                   if idx * self.batchsize + i < self.len_samples]
        assert len(samples) > 0
        samples = [self.getone(i) for i in samples]
        
        out_x, out_y, out_pitch_y = zip(*samples)
        out_x = N.concatenate(out_x)
        out_y = N.concatenate(out_y)
        out_pitch_y = N.concatenate(out_pitch_y)
        
        
        if self.pitch_to_categorical:
            out_pitch_y = KU.to_categorical(out_pitch_y, num_classes=PITCH_MAX)
        else:
            out_pitch_y = out_pitch_y / (PITCH_MAX - 1)
        
        
        return [out_x, out_pitch_y], out_y
        
    def getone(self, i):
        instrument = self.instrument_range[i % len(self.instrument_range)]
        i = i // len(self.instrument_range)
        pitch_y = self.pitch_range[i % len(self.pitch_range)]
        
        if self.fix_input_pitch:
            pitch_x = self.pitch_range[len(self.pitch_range) // 2]
        else:
            i = i // len(self.pitch_range)
            pitch_x = self.pitch_range[i % len(self.pitch_range)]
            
        out_x = self.__generate_one(instrument, pitch_x)
        out_y = self.__generate_one(instrument, pitch_y)
        
        return out_x, out_y, N.array([pitch_y])
            
    
    def __generate_one(self, voice, pitch):
        result = self.data[voice][pitch,:,:]
        result = result[N.newaxis,:,:]
        if self.spectrum_db:
            result = R.amplitude_to_db(result, ref=N.max)
            result = spectrum_regularise(result)
        return result
    
        