import mido
import numpy as N
import sys

from .const import *
from .midi import *
from .analog import *

class Notewise:
    
    def __init__(self, stepsize):
        self.stepsize = stepsize
        
        self.startsymbol = 0
        
        self.length = self.startsymbol + 1
        self.rest_range = [x+self.length for x in range(len(REST_LIST))]
        
        self.length = max(self.rest_range) + 1
        self.on_range = [x+self.length for x in range(PITCH_MAX)]
        
        self.length = max(self.on_range) + 1
        self.off_range = [x+self.length for x in range(PITCH_MAX)]
        
        self.length = max(self.off_range) + 1
    
    def get_startsymbol(self):
        return 0
    
    def __len__(self):
        return self.length
    
    @property
    def length_reduced(self):
        """
        Length of alphabet excluding note off
        """
        return max(self.on_range) + 1
    
    @staticmethod
    def create_representation(stepsize=None):
        n = Notewise(stepsize)
        return n
    
    def encode_track(self, track):
        if type(track) == mido.MidiTrack:
            track = track_absolute_time(track)
        
        # filter all note pairs.
        track = [x for x in track if len(x) == 4]
        # split the note on/off events
        track = [x for t1,msg1,t2,msg2 in track for x in [(t1,msg1),(t2,msg2)]]
        # sort the track by 1. time 2. pitch
        def keyfunc(arg):
            t,msg = arg
            return t * 256 + msg.note
        
        track = sorted(track, key=keyfunc)
        
        result = []
        horizon = 0
        for t,msg in track:
            assert t >= horizon
            wait = (t - horizon) // self.stepsize
            if wait > REST_LIST[-1] * 8:
                sys.stderr.write("Warning: Long wait: {} > {}\n".format(wait, REST_LIST[-1]))
            while wait > 0:
                # find longest duration which fits in the wait
                rest_length = [x for x in REST_LIST if x <= wait][-1]
                result.append(self.e_rest(rest_length))
                wait -= rest_length
            horizon = t
            
            if is_note_on(msg):
                result.append(self.e_note_on(msg.note))
            elif is_note_off(msg):
                result.append(self.e_note_off(msg.note))
            else:
                raise Error("Should not reach this")
        return N.array(result)
    
    
    def to_time_words_state_format(self, encoded, tickrate, to_frame=False):
        """
        Given a list encoded of words which fully encode a track, generate 3 numpy arrays
        1. Activation times
        2. Events happening in each frame
        3. Notes which are on during a particular frame
        
        tickrate = ticks/second
                 = ticks/beat / (microsecond / beat / 1000000)
                 = ticks_per_beat / (TEMPO / 1000000)
                 
        """
        state_on = N.zeros((PITCH_MAX,), dtype='int8')
        result = [(0, [], N.copy(state_on)[N.newaxis,:])]
        horizon = 0
        for x in encoded:
            ty,val = self.decode(x)
            result[-1][1].append(x)
            if ty == 'rest':
                assert val * self.stepsize > 0
                horizon += val * self.stepsize
                assert horizon >= result[-1][0]
                # Can move onto next timeframe
                result.append((horizon, [], N.copy(state_on)[N.newaxis,:]))
            elif ty == 'on':
                state_on[val] = 1
            elif ty == 'off':
                state_on[val] = 0

            # Can move onto next timeframe
            #if len(state_on) == 0:
            #    result.append((horizon, []))
        
        # max number of words in a frame
        times,words,states = zip(*result)
        times = [t / tickrate for t in times]
        if to_frame:
            times = [second_to_frame(t) for t in times]
            times = N.array(times, dtype='int64')
        else:
            times = N.array(times, dtype='float64')
        
        max_words = max([len(w) for w in words])
        def pad_one(wordli):
            n = len(wordli)
            wordli = N.array(wordli, dtype='int')
            if max_words > n:
                wordli = N.pad(wordli, pad_width=((0,max_words-n),), constant_values=MASK_VAL)
            return wordli[N.newaxis,:]

        words = [pad_one(w) for w in words]
        words = N.concatenate(words, axis=0)
        
        states = N.concatenate(states, axis=0)
        
        return times, words, states
        
    
    def encode_track2(self, track):
        """
        Like encode_track, but instead stores note duration as another component
        """
        if type(track) == mido.MidiTrack:
            track = track_absolute_time(track)
        
        # filter all note pairs.
        track = [x for x in track if len(x) == 4]
        # split the note on/off events
        def track_map(arg):
            t1,msg1,t2,msg2 = arg
            assert (t2 - t1) % self.stepsize == 0
            d = (t2 - t1) // self.stepsize
            
            assert msg1.note == msg2.note
            
            return t1,msg1.note,d
        track = [track_map(a) for a in track]
        
        # sort lexicographically.
        track = sorted(track)
        
        result = []
        horizon = 0
        for t,note,duration in track:
            assert t >= horizon
            assert (t - horizon) % self.stepsize == 0
            
            wait = (t - horizon) // self.stepsize
            if wait > DURATION_LIST[-1] * 8:
                sys.stderr.write("Warning: Long wait: {} > {}\n".format(wait, REST_LIST[-1]))
            while wait > 0:
                # find longest duration which fits in the wait
                rest_length = [x for x in REST_LIST if x <= wait][-1]
                result.append([self.e_rest(rest_length), 0])
                wait -= rest_length
            assert wait == 0
            horizon = t
            
            result.append([self.e_note_on(note), duration])
        return result
    
    
    def to_time_words_format2(self, encoded, tickrate, to_frame=False):
        """
        Given a list encoded of words which fully encode a track, generate 3 numpy arrays
        1. Activation times
        2. Events happening in each frame
        
        tickrate = ticks/second
                 = ticks/beat / (microsecond / beat / 1000000)
                 = ticks_per_beat / (TEMPO / 1000000)
                 
        """
        result = [(0, [])]
        horizon = 0
        for x in encoded:
            ty,val = self.decode(x)
            result[-1][1].append(x)
            if ty == 'rest':
                assert val * self.stepsize > 0
                horizon += val * self.stepsize
                assert horizon >= result[-1][0]
                # Can move onto next timeframe
                result.append((horizon, []))

            # Can move onto next timeframe
            #if len(state_on) == 0:
            #    result.append((horizon, []))
        
        # max number of words in a frame
        times,words = zip(*result)
        times = [t / tickrate for t in times]
        if to_frame:
            times = [second_to_frame(t) for t in times]
            times = N.array(times, dtype='int64')
        else:
            times = N.array(times, dtype='float64')
        
        max_words = max([len(w) for w in words])
        def pad_one(wordli):
            n = len(wordli)
            wordli = N.array(wordli, dtype='int')
            if max_words > n:
                wordli = N.pad(wordli, pad_width=((0,max_words-n),), constant_values=MASK_VAL)
            return wordli[N.newaxis,:]

        words = [pad_one(w) for w in words]
        words = N.concatenate(words, axis=0)
        
        return times, words
        
    def e_note_on(self, pitch):
        assert 0 <= pitch and pitch < 128
        return self.on_range[pitch]
    def e_note_off(self, pitch):
        assert 0 <= pitch and pitch < 128
        return self.off_range[pitch]
    
    def e_rest(self, duration):
        index = REST_LIST.index(duration)
        return self.rest_range[index]
    
    def decode(self, x):
        assert x >= 0
        if x == self.startsymbol:
            return ('start', 0)
        elif x in self.on_range:
            return ('on', x - self.on_range[0])
        elif x in self.off_range:
            return ('off', x - self.off_range[0])
        elif x in self.rest_range:
            return ('rest', REST_LIST[x - self.rest_range[0]])
        else:
            raise Error("Invalid category number: {}".format(x))
            
            
    def to_readable(self, x):
        a,b = self.decode(x)
        if a == 'on':
            return '+{}'.format(b)
        elif a == 'off':
            return '-{}'.format(b)
        elif a == 'rest':
            return 'r{}'.format(b)
        elif a == 'start':
            return '<start>'
        else:
            raise Error("should not reach this point")
                    
    def to_string(self, li):
        return ' '.join([self.to_readable(x) for x in li if x >= 0])
    
    def frame_hamming_distances(self, str1, str2):
        """
        Measures the hamming distance between two strings
        """
        pass
        
                    