import mido
import functools
import os, sys, subprocess
import numpy as N
from .const import *



def is_note_on(m):
    """
    Check if the midi message corresponds to a "on" note
    """
    return m.type == 'note_on' and m.velocity > 0
def is_note_off(m):
    """
    Check if the midi message corresponds to a "off" note
    """
    return m.type == 'note_off' or (m.type == 'note_on' and m.velocity == 0)
def is_end_of_track(m):
    return m.type == 'end_of_track'

def midi_tickrate(midi):
    # t/s = t/b * b/ms * ms/s
    return midi.ticks_per_beat * (1000000 / TEMPO)

def track_absolute_time(track, return_meta=False):
    """
    Convert a midi track to (absolute time, msg) format.
    
    Notes will be converted to (time, msg, time, msg) format.
    
    The time attribute on individual messages must be manually assigned.
    """
    
    horizon = 0
    li_abstime = []
    for msg in track:
        horizon += msg.time
        li_abstime.append((horizon, msg.copy()))
        
    #latest = [0 for _ in range(PITCH_MAX)]
    
    li_grouped = []
    while li_abstime:
        
        t,msg = li_abstime.pop(0)
        if is_note_on(msg):
            #assert latest[msg.note] <= t
            # find the next note on/off
            
            anchor = -1
            for i in range(len(li_abstime)):
                t2,msg2 = li_abstime[i]
                if not (is_note_on(msg2) or is_note_off(msg2)):
                    continue
                    
                if msg2.note == msg.note:
                    anchor = i
                    break
                    
            
            if anchor == -1:
                sys.stderr.write("Warning: Unterminated end note: {}\n".format(msg.note))
                msg_off = mido.Message('note_off', channel=msg.channel, note=msg.note, velocity=0)
                assert is_note_off(msg_off)
                assert horizon >= t
                li_grouped.append((t,msg,horizon,msg_off))
                #latest[msg.note] = horizon
            else:
                t2,msg2 = li_abstime[anchor]
            
                if is_note_on(msg2):
                    msg_off = mido.Message('note_off', channel=msg.channel, note=msg.note, velocity=0)
                else:
                    del li_abstime[anchor]
                    msg_off = msg2
                    assert is_note_off(msg_off)
                
                assert t2 >= t
                li_grouped.append((t,msg,t2,msg_off))
                
                #latest[msg.note] = t2
                 
        elif is_note_off(msg):
            sys.stderr.write("Warning: Note off before note on: {}\n".format(msg.note))
            #latest[msg.note] = t
        else:
            li_grouped.append((t,msg))
    
    if return_meta:
        return li_grouped, {"horizon": horizon}
    else:
        return li_grouped

def track_align(track, stepsize, permissible_durations=DURATION_LIST, verbose=False):
    # convert notes to absolute time
    li,meta = track_absolute_time(track, return_meta=True)
    
    # Align all note starting points to stepsize.
    
    permissible_durations = [x * stepsize for x in permissible_durations]
    if verbose:
        print(permissible_durations)
        

    # Must align the notes from the end to the beginning.
    # If two notes overlap, e.g. (0,10), (10,20), since note beginnings must be
    # preserved, the note at position 0 must yield.
    
    pitch_horizons = [meta['horizon'] for _ in range(PITCH_MAX)]
    
    def align_one(m):
        boundary = int(round(m[0] / stepsize)) * stepsize
        
        if len(m) == 4:
            t1,msg1,t2,msg2 = m
            
            # rectify the beginning of the note
            t1 = min(boundary, int(round(t1 // stepsize)) * stepsize)
                
            # rectify the length of the note
            assert (t2 - t1) >= 0
            
            duration = t2 - t1
            duration_list = [x for x in permissible_durations if t1+x <= pitch_horizons[msg1.note]]
            if duration_list:
                duration = min(duration_list, key=lambda x:abs(duration-x))
                assert (duration % stepsize) == 0
                t2 = t1 + duration
                assert (t2 - t1) in permissible_durations
                pitch_horizons[msg1.note] = t1
            else:
                sys.stderr.write("Unable to find duration for transient note {}. Consider inserting a lower permissible duration value. The note will be removed.\n")
                t2 = t1
                
            return (t1,msg1,t2,msg2)
        elif len(m) == 2:
            t1,msg1 = m
            t1 = boundary
            return (t1,msg1)
        
        raise Error("Should not reach this point")
    
    for i in reversed(range(len(li))):
        li[i] = align_one(li[i])
    
    # Remove 0 length notes.
    def filter_msgs(arg):
        if len(arg) == 4:
            return arg[2] - arg[0] > 0
        else:
            return True
    li = [x for x in li if filter_msgs(x)]
    
    
    # Decouple the notes from a 4-tuple to 2 pairs
    def decouple(m):
        if len(m) == 4:
            return [(m[0],m[1]), (m[2],m[3])]
        elif len(m) == 2:
            return [m]
            
        raise Error("Should not reach this point")
        
    sequence = [x for m in li for x in decouple(m)]
    
    # Render the notes
    def keyfunc(arg):
        t,msg = arg
        
        if is_note_off(msg):
            return t*3
        elif is_note_on(msg):
            return t*3+2
        else:
            return t*3+1
        
    sequence = sorted(sequence, key=keyfunc)
    # Compute times
    
    track_out = []
    horizon = 0
    for t,msg in sequence:
        assert t >= horizon
        assert (t % stepsize) == 0
        msg.time = t - horizon
        track_out.append(msg)
        horizon = t
    
    return mido.MidiTrack(track_out)

    
def track_set_properties(track, **kwargs):
    for msg in track:
        if msg.type == 'program_change':
            if "voice" in kwargs:
                msg.program = kwargs["voice"]
        elif msg.type == 'set_tempo':
            if "tempo" in kwargs:
                msg.tempo = kwargs["tempo"]
    return track
                
def track_place_anchors(track):
    track.insert(0, mido.Message("program_change", program=0, time=0, channel=0))
    track.insert(0, mido.MetaMessage("set_tempo", tempo=TEMPO, time=0))
    if track[-1].type != "end_of_track":
        track.append(mido.MetaMessage("end_of_track"))
    return track
    
def track_is_transcribable(track):
    msgs = [msg for msg in track if msg.type == 'note_on']
    programs = [msg.channels for msg in msgs]
    return 10 not in set(programs)

def midi_rectify(midi, step_fraction=STEP_FRACTION, voice=0, verbose=False):
    step_size = get_stepsize(midi.ticks_per_beat, step_fraction)
    print("Stepsize={}".format(step_size))

    midi_out = mido.MidiFile(type=midi.type)
    midi_out.ticks_per_beat = midi.ticks_per_beat
    for track in midi.tracks:
        track_out = track_align(track, stepsize = step_size, permissible_durations=DURATION_LIST, verbose=verbose)
        track_out = track_set_properties(track_place_anchors(track_out), voice=voice, tempo=TEMPO)
        assert track_out
        midi_out.tracks.append(track_out)

    return midi_out

def midi_set_voice(midi, voice):
    midi_out = mido.MidiFile(type=midi.type)
    midi_out.ticks_per_beat = midi.ticks_per_beat
    for track in midi.tracks:
        track_out = track_set_properties(track, voice=voice, tempo=TEMPO)
        assert track_out
        midi_out.tracks.append(track_out)

    return midi_out

def track_activation_times(li, ticks_per_beat):
    if type(li) == mido.MidiTrack:
        li = track_absolute_time(li)
    li = [x for x in li if len(x) == 4]
    def mapone(arg):
        t1,msg1,t2,msg2 = arg
        time_start = t1 / ticks_per_beat * TEMPO / 1000000
        time_end   = t2 / ticks_per_beat * TEMPO / 1000000
        return t1,msg1,t2,msg2,time_start,time_end
    return [mapone(x) for x in li]
    
def track_to_matrices(track, stepsize):
    """
    Convert a MIDI track to two matrices of size (DURATION,PITCH_MAX) which encodes the following information:
    
    status: (t,f) encodes state of note at pitch f at time t. 2=Activated, 1=Sustain, 0=Off
    activate: (t,f) is the length of the note beginning at time t if a note exists. Otherwise 0
    """
    li = track_absolute_time(track)
    li = [x for x in li if len(x) == 4]
    
    horizon = max([x[2] for x in li])
    assert (horizon % stepsize) == 0
    horizon = horizon // stepsize
    
    # 0:Off, 1:Sustain, 2:Attack
    m_status = N.zeros((horizon,PITCH_MAX),dtype='int8')
    m_activate = N.zeros((horizon,PITCH_MAX),dtype='int16')
    
    status = N.zeros((PITCH_MAX,))
    for t1,msg1,t2,msg2 in li:
        assert (t1 % stepsize) == 0
        assert (t2 % stepsize) == 0
        
        t1 = t1 // stepsize
        t2 = t2 // stepsize
        note = msg1.note
        assert note == msg2.note
        assert t2 >= t1
        
        m_activate[t1,note] = t2 - t1
        
        assert m_activate[t1,note] >= 0
        
        m_status[t1,note] = 2
        for i in range(t1+1,t2):
            m_status[i,note] = 1
        
    return m_status, m_activate
        
def midi_single_note(note=60, voice=0, ticks_per_beat=480, duration=240, velocity=70, offset=0):
    assert duration <= ticks_per_beat
    
    midi_out = mido.MidiFile(type=0)
    midi_out.ticks_per_beat = ticks_per_beat
    
    track = [
        mido.Message("program_change", program=voice, time=0, channel=0),
        mido.MetaMessage("set_tempo", tempo=TEMPO, time=0),
        mido.Message("note_on", velocity=velocity, channel=0, note=note, time=offset),
        mido.Message("note_off", velocity=0, channel=0, note=note, time=duration),
        mido.MetaMessage("end_of_track", time=ticks_per_beat-duration),
    ]
    
    track = mido.MidiTrack(track)
    midi_out.tracks.append(track)
    
    return midi_out


# Render
def render_midi(fpath, fpath_out=None):
    if fpath_out is None:
        fpath_out = fpath[:-3] + 'mp3'
    fpath_temp = fpath[:-4].replace('.', '_') + '.wav'
    if os.path.exists(fpath_temp):
        os.remove(fpath_temp)
    if os.path.exists(fpath_out):
        os.remove(fpath_out)
    subprocess.run(['timidity', fpath, '-Ow', '--preserve-silence'])
    assert os.path.exists(fpath_temp)
    subprocess.run(['ffmpeg', '-i', fpath_temp, fpath_out])
    os.remove(fpath_temp)
    #os.rename(fpath_temp, fpath_out)
    return fpath_out