# Number of samples / second
SAMPLE_RATE = 44100
# Number of samples / frame
HOP_LENGTH = 1024
# Number of frames / second
FRAME_RATE = SAMPLE_RATE / HOP_LENGTH

TEMPO=600000
NOTE_MIN='C-1'
PITCH_MAX=128
AUTOENCODER_DURATION=128

# duration before & after the window
WINDOW_LEFT = int(0.5 * FRAME_RATE)
WINDOW_RIGHT = int(2.5 * FRAME_RATE)
WINDOW_WIDTH = WINDOW_LEFT + WINDOW_RIGHT
MASK_VAL = -1
STEP_FRACTION = 32

def get_stepsize(ticks_per_beat, fraction=STEP_FRACTION):
    return int(round(ticks_per_beat / fraction))

DURATION_LIST = [1,2,3,4,6,8,12,16,24,32,48,64,96,128]
REST_LIST = list(range(1,STEP_FRACTION+1))
N_DURATIONS = 1 + len(DURATION_LIST)