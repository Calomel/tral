
import keras.models as KM
import keras.optimizers as KO
import keras.layers as KL
import keras.losses
import keras.regularizers as KR
import keras.initializers as KI
import keras.utils as KU
import keras.backend as KB

import sklearn.model_selection
import sklearn.utils as SU
import sklearn.metrics as SM

import os
import math
import numpy as N
import numpy.random as NR
import librosa as R
import librosa.display as RD

import scipy.io

from matplotlib import pyplot
import random

import tensorflow
import pickle

import tral
import tral.analog
import tral.const
import tral.datagen_transcriber
import tral.notewise
import tral.metrics
import importlib



notewise = tral.notewise.Notewise.create_representation()

with open("mat/lists/MAPS/rectified.pickle", "rb") as f:
    li_rectified = pickle.load(f)
    random.seed(441)
    random.shuffle(li_rectified)
    
    

    
MASK_VAL = -1

def voice_embed_func(v):
    KEYS = [
        [0,0],
        [1,0],
        [1,1],
        [2,1],
        [2,0],
        [0,1],
    ]
    i = tral.instruments.FUNDAMENTAL_INSTRUMENTS.index(v)
    return N.array(KEYS[i])



def build_model(model_name="Transcriber"):
    VOICE_IN_DIM = 2
    CHANNELS_1 = 96
    CHANNELS_2 = 128
    KERNEL = (3,3)
    VOCAB_SIZE = notewise.length_reduced
    
    def get_conv(channels, size, name, padding='same', activation=None):
        return KL.Conv2D(channels, kernel_size=size, \
                      data_format="channels_last", padding=padding, \
                      kernel_regularizer=KR.l2(0.001), \
                      activation=activation, \
                      name=name)
    
    def add_dense(l, n, name, drop=0.2):
        l = KL.Dense(n, kernel_regularizer=KR.l2(0.001), name=name)(l)
        l = KL.LeakyReLU(alpha=0.03, name=name + "_Act")(l)
        l = KL.Dropout(drop, name=name + "_Drop")(l)
        return l
    
    def get_activation(name):
        return KL.ELU(name=name + "_Act")
    
    def channelwise_attention(l_atten, l_src, name):
        n_w = int(l_atten.shape[1])
        n_h = int(l_atten.shape[2])
        n_d = int(l_atten.shape[3])
        assert n_w == int(l_src.shape[1])
        assert n_h == int(l_src.shape[2])
        assert n_d == int(l_src.shape[3])
        
        l = l_atten
        l = KL.Permute((3,1,2), name=name + "_Permute1")(l)
        l = KL.Reshape(target_shape=(n_d,n_w * n_h), name=name + "_Reshape1")(l)
        l = KL.Softmax(name=name+"_Softmax")(l)
        l = KL.Reshape(target_shape=(n_d,n_w,n_h), name=name + "_Reshape2")(l)
        l = KL.Permute((2,3,1), name=name + "_Permute2")(l)
        
        return KL.Multiply(name=name+"_Mult")([l, l_src])
    
    
    
    # Voice input layer
    voice_in = KL.Input(shape=(VOICE_IN_DIM,), name='V_In')

    voice_layer = voice_in
    voice_layer = add_dense(voice_layer, 16, name="V_Deploy")
    voice_layer = KL.Dense(tral.const.WINDOW_WIDTH * tral.const.PITCH_MAX, \
                           name="V_DenseO")(voice_layer)
    voice_layer = KL.LeakyReLU(alpha=0.03, name="V_DenseO_Act")(voice_layer)
    voice_layer = KL.Reshape(target_shape=(tral.const.WINDOW_WIDTH, tral.const.PITCH_MAX,1), \
                             name="V_Reshape_Out")(voice_layer)

    spec_in = KL.Input(shape=(tral.const.WINDOW_WIDTH, tral.const.PITCH_MAX), name="S_In")
    spec_layer = spec_in
    spec_layer = KL.Reshape(target_shape=(tral.const.WINDOW_WIDTH, tral.const.PITCH_MAX,1), \
                       name="S_Reshape_in")(spec_layer)
    
    layer = KL.concatenate([voice_layer, spec_layer], name="SV_Concat")
    
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv1")(layer)
    layer = KL.BatchNormalization(name="Conv1_BatchNorm")(layer)
    layer = get_activation("Conv1")(layer)
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv2")(layer)
    layer = KL.BatchNormalization(name="Conv2_BatchNorm")(layer)
    layer = get_activation("Conv2")(layer)
    layer_skip_0 = layer
    layer = KL.MaxPooling2D((2,2), name="Pool1")(layer)
    
    
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv3")(layer)
    layer = KL.BatchNormalization(name="Conv3_BatchNorm")(layer)
    layer = get_activation("Conv3")(layer)
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv4")(layer)
    layer = KL.BatchNormalization(name="Conv4_BatchNorm")(layer)
    layer = get_activation("Conv4")(layer)
    layer_skip_1 = layer
    layer = KL.MaxPooling2D((2,2), name="Pool2")(layer)
    
    
    layer = get_conv(CHANNELS_2, KERNEL, name="Conv5")(layer)
    layer = KL.BatchNormalization(name="Conv5_BatchNorm")(layer)
    layer = get_activation("Conv5")(layer)
    layer = get_conv(CHANNELS_2, KERNEL, name="Conv6")(layer)
    layer = KL.BatchNormalization(name="Conv6_BatchNorm")(layer)
    layer = get_activation("Conv6")(layer)
    layer_skip_2 = layer
    layer = KL.MaxPooling2D((2,2), name="Pool3")(layer)
    
    
    layer = get_conv(CHANNELS_2, KERNEL, name="Conv7")(layer)
    layer = KL.BatchNormalization(name="Conv7_BatchNorm")(layer)
    layer = get_activation("Conv7")(layer)
    layer = get_conv(CHANNELS_2, KERNEL, name="Conv8")(layer)
    layer = KL.BatchNormalization(name="Conv8_BatchNorm")(layer)
    layer = get_activation("Conv8")(layer)
    
    layer = KL.UpSampling2D(name="Up1")(layer)
    layer = KL.concatenate([layer, layer_skip_2], name="Skip2")
    
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv9")(layer)
    layer = KL.BatchNormalization(name="Conv9_BatchNorm")(layer)
    layer = get_activation("Conv9")(layer)
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv10")(layer)
    layer = KL.BatchNormalization(name="Conv10_BatchNorm")(layer)
    layer = get_activation("Conv10")(layer)
                     
    layer = KL.UpSampling2D(name="Up2")(layer)
    layer = KL.concatenate([layer, layer_skip_1], name="Skip1")
    
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv11")(layer)
    layer = KL.BatchNormalization(name="Conv11_BatchNorm")(layer)
    layer = get_activation("Conv11")(layer)
    layer = get_conv(CHANNELS_1, KERNEL, name="Conv12")(layer)
    layer = KL.BatchNormalization(name="Conv12_BatchNorm")(layer)
    encoder_layer = layer
    
    #layer = get_activation("Conv12")(layer)
    layer = KL.UpSampling2D(name="Up3")(layer)
    layer = get_conv(1, (3,3), name="ConvA", activation='sigmoid')(layer)
    layer = KL.Multiply(name="Skip0")([layer, spec_layer])
    ae_layer = layer
    
    ae_layer = KL.Reshape(target_shape=(tral.const.WINDOW_WIDTH, tral.const.PITCH_MAX), name="AE_Out")(ae_layer)
    
    # Export
    
    layer = encoder_layer
    layer = get_conv(8, (5,5), name="SConv1")(layer)
    layer = KL.BatchNormalization(name="SConv1_BatchNorm")(layer)
    layer = get_activation("SConv1")(layer)
    layer = KL.MaxPooling2D((2,2), name="SPool1")(layer)
    layer = KL.Flatten(name="Flatten")(layer)
    layer = add_dense(layer, 256, "Dense1")
    layer = add_dense(layer, 512, "Dense2")
    
    note_state_layer = KL.Dense(tral.const.PITCH_MAX, activation='sigmoid', name="State_Out")(layer)
    
    
    model_pre = KM.Model(inputs=[voice_in, spec_in], \
                         outputs=ae_layer,
                         name=model_name + "_pre")
    model_tr =  KM.Model(inputs=[voice_in, spec_in], \
                         outputs=note_state_layer, \
                         name=model_name)
    return model_pre, model_tr

model_pre, model_tr = build_model(model_name='transcriber2_0318-ao')
#model_pre.summary()
#model_tr.summary()

def voice_embed_func(v):
    KEYS = [
        [0,0],
        [1,0],
        [1,1],
        [2,1],
        [2,0],
        [0,1],
    ]
    i = tral.instruments.FUNDAMENTAL_INSTRUMENTS.index(v)
    return N.array(KEYS[i])

li_train = li_rectified[len(li_rectified)//10:]
li_val = li_rectified[:len(li_rectified)//10]

def load_samples():
    li_train = li_rectified[len(li_rectified)//10:]
    li_val = li_rectified[:len(li_rectified)//10]

    reduction = 2
    li_train = li_train[:len(li_train) // reduction]
    li_val = li_val[:len(li_val) // reduction]

    importlib.reload(tral.datagen_transcriber)
    kwargs = {
        'range_voice': tral.instruments.FUNDAMENTAL_INSTRUMENTS,
        'batchsize': 16,
        'cache_size': 256,
        'vocab_size': notewise.length_reduced,
        'use_recombination': True,
        'voice_embed_func': voice_embed_func,
        'randomise_index': True,
        'notewise': notewise,
        'generation_mode': 'autoencoder',
        'weight_mode': 'size',
        'pitch_only': True,
        'batch_cache_dir': None,
    }
    train_gen = tral.datagen_transcriber.DataGenTranscriber(filenames=li_train, **kwargs)
    val_gen = tral.datagen_transcriber.DataGenTranscriber(filenames=li_val, **kwargs)
    print(len(li_train))
    print(len(li_val))
    
    return train_gen, val_gen

train_gen, val_gen = load_samples()


def pretrain():
    train_gen.generation_mode = 'autoencoder'
    val_gen.generation_mode = 'autoencoder'

    def ae_loss(y_true, y_pred):
        loss_mse = keras.losses.mean_squared_error(y_true, y_pred)
        loss_mae = keras.losses.mean_absolute_error(y_true, y_pred)
        return 256 * (3 * loss_mse + loss_mae)
    model_pre.compile(
        optimizer=KO.Adam(0.0005),
        loss=ae_loss,
        metrics=['mse', 'mae', ae_loss])
    history = model_pre.fit_generator(
        generator=train_gen, validation_data=val_gen,
        callbacks=tral.metrics.callbacks(model_pre.name),
        workers=4, use_multiprocessing=False, max_queue_size=8,
        epochs=5)
    
def train():
    train_gen.generation_mode = 'multilabel'
    val_gen.generation_mode = 'multilabel'
    
    class_weights = train_gen.generate_class_weights()
    
    train_gen.randomise_index = True
    val_gen.randomise_index = True
    train_gen.use_recombination = True
    val_gen.use_recombination = True

    weighted_bce = tral.metrics.get_weighted_binary_crossentropy(class_weights)
    model_tr.compile(
        optimizer=KO.Adam(),
        loss=weighted_bce,
        metrics=['binary_accuracy'],
    )
    history = model_tr.fit_generator(
        generator=train_gen, validation_data=val_gen,
        callbacks=tral.metrics.callbacks(model_tr.name),
        workers=4, use_multiprocessing=False, max_queue_size=16,
        epochs=10)
    tral.metrics.plot_history(history.history, 'loss')
    tral.metrics.plot_history(history.history, 'binary_accuracy')
    
pretrain()
train()